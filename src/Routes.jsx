import React, { Suspense } from 'react'

// Dependencias Internas
import Home from './components/App'
import Loading from './components/layout/loading/Loading'

//Dependencias Externas
import { BrowserRouter, Switch, Route } from 'react-router-dom'

const Obrigado = React.lazy(() => import('./components/obrigado/Obrigado'));

export default props =>
    <Suspense fallback={<Loading />}>
        <BrowserRouter>
            <Switch>
                <Route path="/" exact={true} component={Home} />
                <Route path="/obrigado" exact={true} component={Obrigado} />
            </Switch>
        </BrowserRouter>
    </Suspense>

import React, { useState, useEffect } from 'react'
import './CadastrarMotorista.css'


import { useHistory } from "react-router-dom";
import { cpf, cnpj } from 'cpf-cnpj-validator'
import { Formik, Field } from 'formik'
import axios from 'axios'
import api from '../../services/Api'
import { CEP, CNPJ, Telefone, CPF } from '../../services/Mask'
import JsonFile from '../../assets/Bancos.json'
import TermosCondicoes from '../../assets/BBMU_Termos_Condicoes.pdf'

export default props => {
    let history = useHistory();
    const SIZE_FILE = 2
    const [bancos] = useState(JsonFile)
    const [modelos, setModelos] = useState(false)
    const [categorias, setCategorias] = useState(false)
    const [errorCategoria, setErrorCategoria] = useState(false)
    const [vetCategorias, setVetCategorias] = useState([])
    const [error, setError] = useState(false)
    const [errorMessage, setErrorMessage] = useState('')
    const [nomeInteiro, setNomeInteiro] = useState(false)
    const [isValidCpf, setIsValidCpf] = useState(null)
    const [isValidCpfTitular, setIsValidCpfTitular] = useState(null)
    const [errorImage, setErrorImage] = useState(false)

    useEffect(() => {
        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString)

        if (urlParams.get('hash')) {
            api.get(`/web/driver/${urlParams.get('hash')}/name`)
                .then(response => {
                    let firstName = response.data.firstName
                    let lastName = response.data.lastName
                    setNomeInteiro(`${firstName} ${lastName}`)
                })
        }

        api.get('/web/service')
            .then(response => setCategorias(response.data))

        api.get('/car')
            .then(response => setModelos(response.data.cars))
    }, [])

    const onChangeCEP = (event, setFieldValue) => {
        const cep = event.target.value
        if (cep.length >= 9) {
            axios.get(`https://viacep.com.br/ws/${cep}/json`)
                .then(response => {
                    setFieldValue('cep', cep)
                    setFieldValue('logradouro', response.data.logradouro)
                    setFieldValue('cidade', response.data.localidade)
                    setFieldValue('estado', response.data.uf)
                })
        }
    }

    const categoriasHandler = (event) => {
        let id = event.target.id
        let checked = event.target.checked

        if (checked) {
            var categoriaSelecionada = {
                "id": id
            }
            let aux_vetCategorias = [...vetCategorias]
            aux_vetCategorias.push(categoriaSelecionada)
            setVetCategorias(aux_vetCategorias)
        }
        else {
            var aux_vetCategorias = [...vetCategorias]
            let filterVetCategorias = aux_vetCategorias.filter(categoria => categoria.id !== id)
            setVetCategorias(filterVetCategorias)
        }
        document.getElementById(`${id}`).checked = checked;

    }

    const uploadImages = async (file, inputName) => {
        const formData = new FormData();
        formData.append(inputName, file);

        const config = { headers: { 'Content-Type': 'multipart/form-data', 'type': 'document' } };
        let response = await api.post('/web/upload', formData, config)

        return response.data.id
    }

    const validatePerfil = (file) => {
        if (file.size)
            var filesize = ((file.size / 1024) / 1024).toFixed(4);

        if (filesize > SIZE_FILE)
            return false
        if (file.type !== 'image/jpeg' && file.type !== 'image/png' && file.type !== 'image/gif')
            return false


        return true
    }


    return (
        <section className="cadastrar-motorista">
            <div className="container">
                <div className="row row-form">
                    <div className="col-12 col-sm-12 col-md-10 col-lg-8 col-xl-8">
                        <Formik
                            initialValues={{
                                indicadoPor: nomeInteiro ? nomeInteiro : '',
                                email: '',
                                cpf: '',
                                cpfTitular: '',
                                telefone: '',
                                firstName: '',
                                lastName: '',
                                genero: 'M',
                                CNH: '',
                                nomeTitular: '',
                                banco: '001',
                                agencia: '',
                                conta: '',
                                cep: '',
                                logradouro: '',
                                cidade: '',
                                numero: '',
                                complemento: '',
                                estado: '',
                                numeroPlaca: '',
                                corVeiculo: '',
                                anoProducao: '',
                                modelo: modelos ? modelos[0].id : '',
                                tipoConta: 'CP',
                                tipoCarro: 'proprio',
                                ear: '',
                                cnhCateogria: '',
                            }}
                            validate={values => {

                            }}
                            onSubmit={async (values, { setSubmitting }) => {
                                if (vetCategorias.length === 0) {
                                    setErrorCategoria(true)
                                    document.getElementById('categorias').scrollIntoView()
                                }
                                else {
                                    setSubmitting(true)
                                    let gender
                                    if (values.genero === 'M') {
                                        gender = 'male'
                                    } else if (values.genero === 'F') {
                                        gender = 'female'
                                    } else {
                                        gender = 'unkown'
                                    }

                                    let idDriveImage = await uploadImages(values.fotoSelfie, 'driver image')
                                    let idFotoCnh = await uploadImages(values.fotoCnh, 'cnh')
                                    let idFotoSelfieCnh = await uploadImages(values.fotoSelfieCnh, 'selfie')
                                    let idFotoVeiculo = await uploadImages(values.fotoVeiculo, 'car image')
                                    let idfotoDocumento = await uploadImages(values.fotoDocumento, 'car document')

                                    if (values.contratoLocacao)
                                        var idContratoLocacao = await uploadImages(values.contratoLocacao, 'car location contract')
                                    let idComprovanteResidencia = await uploadImages(values.comprovanteResidencia, 'proof of address')


                                    const data = {
                                        "driver": {
                                            "carId": values.modelo.toString(),
                                            "carPlate": values.numeroPlaca,
                                            "address": values.logradouro,
                                            "gender": gender,
                                            "firstName": values.firstName,
                                            "lastName": values.lastName,
                                            "carColor": values.corVeiculo,
                                            "certificateNumber": values.CNH,
                                            "carProductionYear": values.anoProducao.toString(),
                                            "mobileNumber": `55${values.telefone.replace(/\D/g, '')}`,
                                            "carStatus": values.tipoCarro === 'alugado' ? true : false,
                                            "email": values.email,
                                            "driverImage": idDriveImage,
                                            "documents": [
                                                {
                                                    "id": idFotoCnh
                                                },
                                                {
                                                    "id": idFotoSelfieCnh
                                                },
                                                {
                                                    "id": idFotoVeiculo
                                                },
                                                {
                                                    "id": idfotoDocumento
                                                },
                                                {
                                                    "id": idContratoLocacao
                                                },
                                                {
                                                    "id": idComprovanteResidencia
                                                },
                                            ],
                                            "services": vetCategorias,
                                            "cpf": values.cpf,
                                            "cep": values.cep,
                                            "number": values.numero,
                                            "city": values.cidade,
                                            "state": values.estado,
                                            "cnhEar": values.ear,
                                            "cnhCategory": values.cnhCateogria,
                                            "cnhExpirationDate": values.dataValidadeCnh
                                        },
                                        "bankAccount": {
                                            "holder": values.nomeTitular,
                                            "cpf": values.cpfTitular,
                                            "bankCode": values.banco,
                                            "agencyCode": values.agencia,
                                            "accountNumber": values.conta,
                                            "type": values.tipoConta === 'CP' ? 'savings' : 'checking'
                                        },
                                        "invitationCodeReceived": values.indicadoPor,
                                        "origin": "web"
                                    }

                                    api.post('/web/driver', data)
                                        .then(response => {
                                            setSubmitting(false)
                                            history.push('/obrigado')
                                        }).catch(err => {
                                            setError(true)
                                            setErrorMessage(err.response.data.message)
                                            setSubmitting(false)
                                        })
                                }

                            }}
                        >
                            {({
                                values,
                                handleSubmit,
                                handleChange,
                                isSubmitting,
                                setFieldValue
                            }) => (
                                    <form onSubmit={handleSubmit}>
                                        {error &&
                                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                {errorMessage}.
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                        }
                                        <div className="card">
                                            <div className="card-header">
                                                <i className="fas fa-link"></i> Indicação
                                            </div>
                                            <div className="card-body">
                                                <div className="row">
                                                    <div className="col-md-12">
                                                        <div className="form-group">
                                                            <label htmlFor="indicadoPor">Indicado por</label>
                                                            <Field type="text" name="indicadoPor" className="form-control" disabled required />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="card">
                                            <div className="card-header">
                                                <i className="fas fa-phone-alt"></i> Contato
                                            </div>
                                            <div className="card-body">
                                                <div className="row">
                                                    <div className="col-md-6">
                                                        <div className="form-group">
                                                            <label htmlFor="email">E-mail</label>
                                                            <Field type="email" name="email" placeholder="E-mail" className="form-control" required />
                                                        </div>
                                                    </div>
                                                    <div className="col-md-6">
                                                        <div className="form-group">
                                                            <label htmlFor="telefone">Telefone</label>
                                                            <Field type="text" name="telefone" placeholder="Telefone" onChange={(event) => {
                                                                event.target.value = Telefone(event.target.value);
                                                                handleChange(event)
                                                            }} className="form-control" required />
                                                            <small className="text-muted">* Esse será o número utilizado no aplicativo</small>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="card">
                                            <div className="card-header">
                                                <i className="fas fa-user"></i>  Dados Pessoais
                                            </div>
                                            <div className="profile-image-upload">
                                                <div className="profile-icon">
                                                    <i className="fas fa-portrait"></i>
                                                </div>
                                                <div className="form-group">
                                                    <label htmlFor="fotoSelfie">Foto do Perfil</label>
                                                    <input id="file" className="form-control-file" name="fotoSelfie" type="file" onChange={(event) => {
                                                        setFieldValue("fotoSelfie", event.currentTarget.files[0]);
                                                        let validate = validatePerfil(event.currentTarget.files[0])
                                                        !validate ? setErrorImage(true) : setErrorImage(false)
                                                    }} required />
                                                </div>
                                                {errorImage &&
                                                    <p className="text-danger">Tipo de arquivo diferente de JPEG, PNG, GIF ou tamanho do arquivo maior que 2MB.</p>
                                                }
                                            </div>

                                            <div className="card-body">
                                                <div className="row">
                                                    <div className="col-md-6">
                                                        <div className="form-group">
                                                            <label htmlFor="firstName">Primeiro Nome</label>
                                                            <Field type="text" name="firstName" placeholder="Primeiro Nome" className="form-control" required />
                                                        </div>
                                                    </div>
                                                    <div className="col-md-6 mb-2">
                                                        <div className="form-group">
                                                            <label htmlFor="lastName">Último Nome</label>
                                                            <Field type="text" name="lastName" placeholder="Último Nome" className="form-control" required />
                                                        </div>
                                                    </div>
                                                    <div className="col-md-4 mb-2">
                                                        <label htmlFor="lastName">Gênero</label>
                                                        <Field name="genero" as="select" placeholder="Gênero" className="form-control" required>
                                                            <option value="M">Masculino</option>
                                                            <option value="F">Feminino</option>
                                                            <option value="N">Outros</option>
                                                        </Field>
                                                    </div>
                                                    <div className="col-md-8">
                                                        <label htmlFor="cpf">CPF</label>
                                                        <Field type="text" name="cpf" placeholder="CPF"
                                                            className={`form-control ${isValidCpf !== null ? (isValidCpf ? 'is-valid' : 'is-invalid') : ''}`}
                                                            onChange={(event) => {
                                                                event.target.value = CPF(event.target.value);
                                                                let isValid = cpf.isValid(event.target.value);
                                                                setIsValidCpf(isValid)
                                                                handleChange(event);
                                                            }} required />
                                                    </div>
                                                    <div className="col-md-8 mb-2">
                                                        <label htmlFor="cnh">CNH</label>
                                                        <Field type="text" name="CNH" placeholder="CNH" className="form-control" required />
                                                    </div>
                                                    <div className="col-md-4 mb-2">
                                                        <label htmlFor="ear">EAR</label>
                                                        <Field type="text" name="ear" placeholder="EAR" className="form-control" required />
                                                    </div>
                                                    <div className="col-md-6 mb-2">
                                                        <label htmlFor="cnhCategoria">Categoria Habilitação</label>
                                                        <Field type="text" name="cnhCategoria" placeholder="Categoria Habilitação" className="form-control" required />
                                                    </div>
                                                    <div className="col-md-6 mb-2">
                                                        <label htmlFor="dataValidadeCnh">Data Validade CNH</label>
                                                        <input type="date" name="dataValidadeCnh" placeholder="Data Validade da CNH" onChange={(event) => setFieldValue("dataValidadeCnh", event.target.value)} className="form-control" required />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="card">
                                            <div className="card-header">
                                                <i className="fas fa-money-check-alt"></i> Dados Bancários
                                            </div>
                                            <div className="card-body">
                                                <div className="row">
                                                    <div className="col-md-6">
                                                        <div className="form-group">
                                                            <label htmlFor="nomeTitular">Nome Completo do Titular</label>
                                                            <Field type="text" name="nomeTitular" placeholder="Nome Completo do Titular" className="form-control" required />
                                                        </div>
                                                    </div>
                                                    <div className="col-md-6">
                                                        <label htmlFor="cpfTitular">CPF/CNPJ do Titular</label>
                                                        <Field type="text" name="cpfTitular" placeholder="CPF do Titular"
                                                            className={`form-control ${isValidCpfTitular !== null ? (isValidCpfTitular ? 'is-valid' : 'is-invalid') : ''}`}
                                                            onChange={(event) => {
                                                                if (event.target.value.length >= 15) {
                                                                    event.target.value = CNPJ(event.target.value);
                                                                    let isValid = cnpj.isValid(event.target.value);
                                                                    setIsValidCpfTitular(isValid)
                                                                }
                                                                else {
                                                                    event.target.value = CPF(event.target.value);
                                                                    let isValid = cpf.isValid(event.target.value);
                                                                    setIsValidCpfTitular(isValid)
                                                                }
                                                                handleChange(event);
                                                            }} required />
                                                    </div>
                                                    <div className="col-md-6">
                                                        <div className="form-group">
                                                            <label htmlFor="banco">Banco</label>
                                                            <Field name="banco" as="select" placeholder="Banco" className="form-control" required>
                                                                {bancos &&

                                                                    bancos.map(banco =>
                                                                        <option value={banco.value} key={banco.value}>{banco.value} - {banco.label}</option>
                                                                    )
                                                                }
                                                            </Field>
                                                        </div>
                                                    </div>
                                                    <div className="col-md-6">
                                                        <div className="form-group">
                                                            <label htmlFor="tipoConta">Tipo da Conta</label>
                                                            <Field name="tipoConta" as="select" placeholder="Tipo da Conta" className="form-control" required>
                                                                <option value="CP">Conta Poupança</option>
                                                                <option value="CC">Conta Corrente</option>
                                                            </Field>
                                                        </div>
                                                    </div>
                                                    <div className="col-md-4">
                                                        <div className="form-group">
                                                            <div className="form-group">
                                                                <label htmlFor="agencia">Agência</label>
                                                                <Field type="text" name="agencia" placeholder="Agência" className="form-control" required />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="col-md-8">
                                                        <div className="form-group">
                                                            <div className="form-group">
                                                                <label htmlFor="conta">Número da Conta + Dígito</label>
                                                                <Field type="text" name="conta" placeholder="Número da Conta" className="form-control" required />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="card">
                                            <div className="card-header">
                                                <i className="fas fa-map-marked-alt"></i> Endereço
                                            </div>
                                            <div className="card-body">
                                                <div className="row">
                                                    <div className="col-md-4">
                                                        <div className="form-group">
                                                            <label htmlFor="cep">CEP</label>
                                                            <Field type="text" name="cep"
                                                                placeholder="CEP"
                                                                onChange={(event) => {
                                                                    onChangeCEP(event, setFieldValue)
                                                                    event.target.value = CEP(event.target.value);
                                                                    handleChange(event);
                                                                }}
                                                                className="form-control" required />
                                                        </div>
                                                    </div>
                                                    <div className="col-md-6">
                                                        <div className="form-group">
                                                            <label htmlFor="logradouro">Logradouro</label>
                                                            <Field type="text" name="logradouro" placeholder="Logradouro" className="form-control" disabled={(values.logradouro === '' && values.cidade === '') ? true : false} required />
                                                        </div>
                                                    </div>
                                                    <div className="col-md-2">
                                                        <div className="form-group">
                                                            <label htmlFor="numero">Número</label>
                                                            <Field type="text" name="numero" placeholder="Número" className="form-control" required />
                                                        </div>
                                                    </div>
                                                    <div className="col-md-4">
                                                        <div className="form-group">
                                                            <label htmlFor="complemento">Complemento</label>
                                                            <Field type="text" name="complemento" placeholder="Complemento" className="form-control" />
                                                        </div>
                                                    </div>
                                                    <div className="col-md-4">
                                                        <div className="form-group">
                                                            <label htmlFor="cidade">Cidade</label>
                                                            <Field type="text" name="cidade" placeholder="Cidade" className="form-control" disabled={values.cidade === '' ? true : false} required />
                                                        </div>
                                                    </div>
                                                    <div className="col-md-4">
                                                        <div className="form-group">
                                                            <label htmlFor="estado">Estado</label>
                                                            <Field name="estado" as="select" placeholder="Estado" className="form-control" disabled={values.estado === '' ? true : false} required>
                                                                <option value="AC">Acre</option>
                                                                <option value="AL">Alagoas</option>
                                                                <option value="AP">Amapá</option>
                                                                <option value="AM">Amazonas</option>
                                                                <option value="BA">Bahia</option>
                                                                <option value="CE">Ceará</option>
                                                                <option value="DF">Distrito Federal</option>
                                                                <option value="ES">Espirito Santo</option>
                                                                <option value="GO">Goiás</option>
                                                                <option value="MA">Maranhão</option>
                                                                <option value="MS">Mato Grosso do Sul</option>
                                                                <option value="MT">Mato Grosso</option>
                                                                <option value="MG">Minas Gerais</option>
                                                                <option value="PA">Pará</option>
                                                                <option value="PB">Paraíba</option>
                                                                <option value="PR">Paraná</option>
                                                                <option value="PE">Pernambuco</option>
                                                                <option value="PI">Piauí</option>
                                                                <option value="RJ">Rio de Janeiro</option>
                                                                <option value="RN">Rio Grande do Norte</option>
                                                                <option value="RS">Rio Grande do Sul</option>
                                                                <option value="RO">Rondônia</option>
                                                                <option value="RR">Roraima</option>
                                                                <option value="SC">Santa Catarina</option>
                                                                <option value="SP">São Paulo</option>
                                                                <option value="SE">Sergipe</option>
                                                                <option value="TO">Tocantins</option>
                                                            </Field>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="card">
                                            <div className="card-header">
                                                <i className="fas fa-car-side"></i> Dados do Veículo
                                            </div>
                                            <div className="card-body">
                                                <div className="row">

                                                    <div className="col-md-12">
                                                        <div className="form-group">
                                                            <label htmlFor="numeroPlaca">Número da Placa</label>
                                                            <Field type="text" name="numeroPlaca" placeholder="Número da Placa" className="form-control" required />
                                                        </div>
                                                    </div>
                                                    <div className="col-md-6">
                                                        <div className="form-group">
                                                            <label htmlFor="corVeiculo">Cor do Veículo</label>
                                                            <Field type="text" name="corVeiculo" placeholder="Cor do Veículo" className="form-control" required />
                                                        </div>
                                                    </div>
                                                    <div className="col-md-6">
                                                        <div className="form-group">
                                                            <label htmlFor="anoProducao">Ano de Produção</label>
                                                            <Field type="number" name="anoProducao" placeholder="Ano de Produção" className="form-control" min="1900" required />
                                                        </div>
                                                    </div>
                                                    <div className="col-md-12">
                                                        <div className="form-group">
                                                            <label htmlFor="modelo">Modelo</label>
                                                            <Field name="modelo" as="select" placeholder="Modelo" className="form-control" required>
                                                                {modelos && modelos.map(modelo =>
                                                                    <option value={modelo.id} key={modelo.id}>{modelo.title}</option>
                                                                )}
                                                            </Field>
                                                        </div>
                                                    </div>
                                                    <div className="col-md-12">
                                                        <div className="form-group mb-0" id="categorias">
                                                            <label htmlFor="categoria">Categorias</label>
                                                            <div className="row">
                                                                {categorias && categorias.map((categoria, index) =>
                                                                    <div className="col-12 col-sm-12 col-md-6 col-lg-4" key={index}>
                                                                        <div className="card card-categoria-carro">
                                                                            <div className="row no-gutters">
                                                                                <div className="col-8 col-sm-8 col-md-8 d-flex align-items-center">
                                                                                    <div className="card-body">
                                                                                        <h5 className="card-title">{categoria.title}</h5>
                                                                                    </div>
                                                                                </div>
                                                                                <div className="col-4 col-sm-4 col-md-4 d-flex justify-content-center align-items-center">
                                                                                    <div className="custom-control custom-checkbox">
                                                                                        <input type="checkbox" id={categoria.id} className="custom-control-input" onChange={categoriasHandler} />
                                                                                        <label className="custom-control-label" htmlFor={categoria.id}></label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                )}
                                                            </div>
                                                        </div>
                                                        {errorCategoria &&
                                                            <p className="text-danger">Selecione ao menos uma categoria para prosseguir</p>
                                                        }
                                                    </div>
                                                    <div className="col-md-12">
                                                        <Field name="tipoCarro" as="select" className="form-control" required>
                                                            <option value="proprio">Próprio</option>
                                                            <option value="alugado">Alugado</option>
                                                        </Field>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div className="card documentos">
                                            <div className="card-header">
                                                <i className="fas fa-id-card-alt"></i>  Documentos
                                            </div>
                                            <div className="card-body">
                                                <div className="row">
                                                    {values.tipoCarro === 'alugado' &&
                                                        <div className="col-md-12">
                                                            <div className="card" >
                                                                <div className="row no-gutters">
                                                                    <div className="col-3 col-sm-3 col-md-3 card-icon">
                                                                        <i class="fas fa-home"></i>
                                                                    </div>
                                                                    <div className="col-9 col-sm-9 col-md-9 card-body">
                                                                        <h5 className="card-title">Contrato de Locação</h5>
                                                                        <p className="card-text">
                                                                            Envie uma foto do contrato de locação.
                                                                    </p>
                                                                        <div className="form-group">
                                                                            <label htmlFor="contratoLocacao"></label>
                                                                            <input id="file" className="form-control-file" name="contratoLocacao" type="file" onChange={(event) => {
                                                                                setFieldValue("contratoLocacao", event.currentTarget.files[0]);
                                                                            }} required />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    }
                                                    <div className="col-md-12">
                                                        <div className="card" >
                                                            <div className="row no-gutters">
                                                                <div className="col-3 col-sm-3 col-md-3 card-icon">
                                                                    <i className="fas fa-id-card"></i>
                                                                </div>
                                                                <div className="col-9 col-sm-9 col-md-9 card-body">
                                                                    <h5 className="card-title">Foto da CNH</h5>
                                                                    <p className="card-text">
                                                                        Envie uma foto da sua Carteira Nacional de Habilitação.
                                                                    </p>
                                                                    <div className="form-group">
                                                                        <label htmlFor="fotoCnh"></label>
                                                                        <input id="file" className="form-control-file" name="fotoCnh" type="file" onChange={(event) => {
                                                                            setFieldValue("fotoCnh", event.currentTarget.files[0]);
                                                                        }} required />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="col-md-12">
                                                        <div className="card" >
                                                            <div className="row no-gutters">
                                                                <div className="col-3 col-sm-3 col-md-3 card-icon">
                                                                    <i className="fas fa-id-card-alt"></i>
                                                                </div>
                                                                <div className="col-9 col-sm-9 col-md-9 card-body">
                                                                    <h5 className="card-title">Foto Selfie  com CNH</h5>
                                                                    <p className="card-text">
                                                                        Envie uma foto sua no estilo selfie segurando a sua Carteira Nacional de Habilitação.
                                                                    </p>
                                                                    <div className="form-group">
                                                                        <label htmlFor="fotoSelfieCnh"></label>
                                                                        <input id="file" className="form-control-file" name="fotoSelfieCnh" type="file" onChange={(event) => {
                                                                            setFieldValue("fotoSelfieCnh", event.currentTarget.files[0]);
                                                                        }} required />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="col-md-12">
                                                        <div className="card" >
                                                            <div className="row no-gutters">
                                                                <div className="col-3 col-sm-3 col-md-3 card-icon">
                                                                    <i className="fas fa-car"></i>
                                                                </div>
                                                                <div className="col-9 col-sm-9 col-md-9 card-body">
                                                                    <h5 className="card-title">Foto do Veículo</h5>
                                                                    <p className="card-text">
                                                                        Envie uma foto do seu veículo.
                                                                    </p>
                                                                    <div className="form-group">
                                                                        <label htmlFor="fotoVeiculo"></label>
                                                                        <input id="file" className="form-control-file" name="fotoVeiculo" type="file" onChange={(event) => {
                                                                            setFieldValue("fotoVeiculo", event.currentTarget.files[0]);
                                                                        }} required />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="col-md-12">
                                                        <div className="card" >
                                                            <div className="row no-gutters">
                                                                <div className="col-3 col-sm-3 col-md-3 card-icon">
                                                                    <i className="fas fa-pager"></i>
                                                                </div>
                                                                <div className="col-9 col-sm-9 col-md-9 card-body">
                                                                    <h5 className="card-title">Foto do Documento do Veículo</h5>
                                                                    <p className="card-text">
                                                                        Envie uma foto do documento do seu veículo.
                                                                    </p>
                                                                    <div className="form-group">
                                                                        <label htmlFor="fotoDocumento"></label>
                                                                        <input id="file" className="form-control-file" name="fotoDocumento" type="file" onChange={(event) => {
                                                                            setFieldValue("fotoDocumento", event.currentTarget.files[0]);
                                                                        }} required />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="col-md-12">
                                                        <div className="card" >
                                                            <div className="row no-gutters">
                                                                <div className="col-3 col-sm-3 col-md-3 card-icon">
                                                                    <i class="fas fa-home"></i>
                                                                </div>
                                                                <div className="col-9 col-sm-9 col-md-9 card-body">
                                                                    <h5 className="card-title">Comprovante de residência</h5>
                                                                    <p className="card-text">
                                                                        Envie uma foto do seu comprovante de residência.
                                                                    </p>
                                                                    <div className="form-group">
                                                                        <label htmlFor="comprovanteResidencia"></label>
                                                                        <input id="file" className="form-control-file" name="comprovanteResidencia" type="file" onChange={(event) => {
                                                                            setFieldValue("comprovanteResidencia", event.currentTarget.files[0]);
                                                                        }} required />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="card">
                                            <div className="card-header">
                                                <i class="fas fa-file-signature"></i> Termos & Condições
                                            </div>
                                            <div className="card-body">
                                                <div className="row">
                                                    <div className="col-md-12">
                                                        <div className="custom-control custom-checkbox">
                                                            <input type="checkbox" id="termos-condicoes" className="custom-control-input" onChange={categoriasHandler} />
                                                            <label className="custom-control-label" htmlFor="termos-condicoes"></label>
                                                            <span className="termos">Eu li e aceito os <a href={TermosCondicoes} className="link" download>Termos & Condições</a></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="d-flex justify-content-end">
                                            {isSubmitting ?
                                                <button type="submit" className="btn btn-primary" disabled={true}>
                                                    <i class="fas fa-spinner fa-pulse"></i>
                                                </button>
                                                :
                                                <button type="submit" className="btn btn-primary">
                                                    Cadastrar
                                                </button>
                                            }
                                        </div>
                                    </form>
                                )}
                        </Formik>
                    </div>
                </div>
            </div>
        </section >
    )

}
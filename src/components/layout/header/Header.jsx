import React from 'react'
import './Header.css'

import Logo from '../../../assets/logo.png'

export default props =>
    <nav className="navbar navbar-light bg-light">
        <div className="navbar-brand mr-0">
            <img src={Logo} alt="Logo" />
        </div>
    </nav>
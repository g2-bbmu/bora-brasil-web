import React from 'react'
import './Loading.css'

export default props =>
    <section className="loading ">
        <i className="fas fa-spinner fa-pulse"></i>
    </section>
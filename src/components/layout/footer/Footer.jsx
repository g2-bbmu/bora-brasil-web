import React from 'react'
import './Footer.css'

export default props =>
    <footer className="footer">
        <div className="container">
            <div className="row">
                <div className="col-md-4">
                    <div className="copy-icon">
                        <a href="https://iconscout.com/icons/car" target="_blank" rel="noopener noreferrer">
                            Car Icon</a> by < a href="https://iconscout.com/contributors/icograms" >
                            Icograms</a > on < a href="https://iconscout.com" >
                            Iconscout
                    </a>
                    </div>
                </div>
            </div>
        </div>
    </footer>
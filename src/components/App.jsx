import React from 'react'

import Header from './layout/header/Header'
import CadastrarMotorista from './cadastrar/CadastrarMotorista'
import Footer from './layout/footer/Footer'

import 'jquery'
import '@fortawesome/fontawesome-free/css/all.min.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap/dist/js/bootstrap.min.js'

export default props =>
    <>
        <Header />
        <CadastrarMotorista />
        <Footer />
    </>
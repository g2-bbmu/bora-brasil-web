import React from 'react'
import './Steps.css'

import { VerticalTimeline, VerticalTimelineElement } from 'react-vertical-timeline-component';
import 'react-vertical-timeline-component/style.min.css';

export default props =>
    <VerticalTimeline>
        <VerticalTimelineElement
            className="vertical-timeline-element--work"
            date=""
            contentStyle={{ borderTop: '3px solid rgb(236, 106, 54)' }}
            iconStyle={{ background: 'rgb(255, 255, 255)' }}
            icon={<i className="fab fa-android"></i>}
        >
            <p className="vertical-timeline-element-title">Baixe o app para <b>Android</b></p>
        </VerticalTimelineElement>
        <VerticalTimelineElement
            className="vertical-timeline-element--work"
            date=""
            contentStyle={{ borderTop: '3px solid rgb(236, 106, 54)' }}
            iconStyle={{ background: 'rgb(255, 255, 255)' }}
            icon={<i className="fas fa-mobile-alt"></i>}
        >
            <p className="vertical-timeline-element-title">Abra o app e informe seu telefone</p>
        </VerticalTimelineElement>
        <VerticalTimelineElement
            className="vertical-timeline-element--work"
            date=""
            contentStyle={{ borderTop: '3px solid rgb(236, 106, 54)' }}
            iconStyle={{ background: 'rgb(255, 255, 255)' }}
            icon={<i className="fas fa-sms"></i>}
        >
            <p className="vertical-timeline-element-title">
                Você receberá uma mensagem sms para validação do número
            </p>
        </VerticalTimelineElement>
        <VerticalTimelineElement
            className="vertical-timeline-element--work"
            date=""
            contentStyle={{ borderTop: '3px solid rgb(236, 106, 54)' }}
            iconStyle={{ background: 'rgb(255, 255, 255)' }}
            icon={<i className="fas fa-comment-dots"></i>}
        >
            <p className="vertical-timeline-element-title">
                Ao abrir o app você receberá uma mensagem informando que ainda está em aprovação
            </p>
        </VerticalTimelineElement>
        <VerticalTimelineElement
            className="vertical-timeline-element--work"
            date=""
            contentStyle={{ borderTop: '3px solid rgb(236, 106, 54)' }}
            iconStyle={{ background: 'rgb(255, 255, 255)' }}
            icon={< i className="fas fa-user-check"></i>}
        >
            <p className="vertical-timeline-element-title">
                Entre de tempos em tempos no app para saber quando foi aprovado
            </p>
        </VerticalTimelineElement>
        <VerticalTimelineElement
            className="vertical-timeline-element--work"
            date=""
            contentStyle={{ borderTop: '3px solid rgb(236, 106, 54)' }}
            iconStyle={{ background: 'rgb(255, 255, 255)' }}
            icon={<i className="fas fa-map-marked-alt"></i>}
        >
            <p className="vertical-timeline-element-title">
                Após aprovação, ative as permissões de localização, armazenamento e ligação
            </p>
        </VerticalTimelineElement>
        <VerticalTimelineElement
            className="vertical-timeline-element--work"
            date=""
            contentStyle={{ borderTop: '3px solid rgb(236, 106, 54)' }}
            iconStyle={{ background: 'rgb(255, 255, 255)' }}
            icon={<i className="fas fa-wifi"></i>}
        >
            <p className="vertical-timeline-element-title">
                Fique disponível no app
            </p>
        </VerticalTimelineElement>
        <VerticalTimelineElement
            className="vertical-timeline-element--work"
            date=""
            contentStyle={{ borderTop: '3px solid rgb(236, 106, 54)' }}
            iconStyle={{ background: 'rgb(255, 255, 255)' }}
            icon={<i className="fas fa-percentage"></i>}
        >
            <p className="vertical-timeline-element-title">
                Faça suas corridas e ganhe <b>100%</b> pra você
            </p>
        </VerticalTimelineElement>

        <VerticalTimelineElement
            className="vertical-timeline-element--work last"
            iconStyle={{ background: 'rgb(236, 106, 54)', color: '#fff !important' }}
            icon={<i className="fas fa-car-side"></i>}
        />

    </VerticalTimeline >
import React from 'react'
import './Obrigado.css'

import Header from '../layout/header/Header'
import AndroidDownload from '../../assets/android-download.png'
import Steps from './steps/Steps'
import 'react-vertical-timeline-component/style.min.css';

export default props =>
    <>
        <section className="obrigado">
            <Header />
            <div className="container">
                <div className="row d-flex justify-content-center">
                    <div className="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10">
                        <div className="card">
                            <div className="card-body">
                                <div className="vertical-timeline-element-icon bounce-in check-icon">
                                    <i className="fas fa-check"></i>
                                </div>
                                <h4>Cadastro concluído com sucesso</h4>
                                <p>Seu cadastro foi enviado para análise e pode levar algum tempo para ser aprovado</p>
                                <div className="invitation-code">
                                    21312-BFF
                                </div>
                                <p>Código de Indicação</p>
                                Enquanto isso, você pode preparar o aplicativo:
                            </div>
                        </div>
                        <Steps />
                        <div className="download-app">
                            <div className="container">
                                <div className="row">
                                    <div className="col-12 col-sm-12 col-md-12 d-flex justify-content-center mb-2">
                                        <img src={AndroidDownload} alt="Botão para fazer Download no Android" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </>
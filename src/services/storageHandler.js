
/* GETTERS */
export const isAuthenticated = () => localStorage.getItem('token') !== null;
export const getToken = () => localStorage.getItem('token');
export const getEquipamento = () => {
    let equipamento = localStorage.getItem('equipamento');
    equipamento = JSON.parse(equipamento);

    return equipamento
}
export const getAnaliseId = () => localStorage.getItem('analiseId');
export const getRole = () => localStorage.getItem('role');


/* SETTERS */
export const loginFunc = (token) => {
    localStorage.setItem('token', token);
};

export const roleFunc = (role) => {
    localStorage.setItem('role', role);
};

export const analiseFunc = (id) => {
    localStorage.setItem('analiseId', id);
}

export const equipamentoFunc = (equipamento) => {
    equipamento = JSON.stringify(equipamento);
    localStorage.setItem('equipamento', equipamento)
}


export const logoutFunc = (props) => {
    localStorage.removeItem('token');
    localStorage.removeItem('analiseId');
    localStorage.removeItem('equipamento');
    localStorage.removeItem('role');
    window.location.href = '/';
};

export const removeStorage = () => {
    localStorage.removeItem('token');
    localStorage.removeItem('analiseId');
    localStorage.removeItem('equipamento');
    localStorage.removeItem('role');
}
